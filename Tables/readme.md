# Data tables

Used to make the analysis.

- Table 1: List of elements (protein, recombination site) found in conjugative element of type T, with corresponding functional annotation when available.
- Table 2: Summary information per element (ICE or CP).
- Table 3: List of pairs of elements and corresponding information (e.g., wGRR, phylogenetic distance between the hosts).
- Table 4: Same as Table 3 but here wGRR is computed without MPF genes.

