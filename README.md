# Host range expansion and genetic plasticity drive the trade-off between integrative and extrachromosomal mobile genetic elements

This repository contains all the supplementary data necessary to reproduce the results from the paper *Host range expansion and genetic plasticity drive the trade-off between integrative and extrachromosomal mobile genetic elements*

A Jupyter notebook allowing to reproduce al the main figure is available.
It gives also a preview of the tables in a nicer format than a raw text file.

Missing figures in the notebook: S1, S2, S5 and S6.
Coming soon.

